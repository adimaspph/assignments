package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * convert string into ArrasList.
     * @param code string bit
     * @return arraylist
     */
    public static ArrayList stringToList(String code) {
        ArrayList<Integer> listCode = new ArrayList<Integer>(code.length());

        for (int i = 0; i < code.length(); i++) {
            int eachDigit = code.charAt(i) - '0';
            listCode.add(i, eachDigit);
        }
        return listCode;
    }

    /**
     * Mencari banyaknya redundan dari bit asli.
     * @param code bit asli
     * @return int banyaknya redundan
     */
    public static int findRedundan(String code) {
        int count = 0;
        while (true) {
            if (Math.pow(2, count) >= code.length() + count + 1) {
                break;
            }
            count++;
        }
        return count;
    }

    /**
     * Mencari Hammingcode dari bit asli.
     * @param data (bit asli)
     * @return string hammingcode
     */
    public static String encode(String data) {
        //Membuat String menjadi ArrayList
        ArrayList<Integer> listCode = new ArrayList<Integer>();
        listCode = stringToList(data);

        //find amount of redundan
        int redundan = findRedundan(data);
        
        //menambah redundan bit kedalam list
        for (int i = 0; i < redundan; i++) {
            double p = Math.pow(2, i);
            listCode.add((int)p - 1, 0);
        }

        int resultParity;
        int index;
        int start;

        //looping sebanyak redundan
        for (int r = 0, jarak = 1; r < redundan; r++, jarak *= 2) {
            //Restart result
            resultParity = 0;

            //inisiasi index
            start = (int)Math.pow(2, r);
            index = start - 1;

            //Menghitung Parity
            while (listCode.size() > index) {
                //Menjumlah parity kedalam result
                for (int i = 0; i < jarak; i++) {
                    if (listCode.size() > index) {
                        resultParity += listCode.get(index);
                        index++;
                    }
                }
                //Skip sejumla jarak yang ingin di skip
                for (int i = 0; i < jarak; i++) {
                    index += 1;
                }
            }
            //Mengubah bit parity, jika genap maka 0, jika ganjil maka 1
            listCode.set(start - 1, resultParity % 2);
        }

        //Mengubah dari Arraylist menjadi String
        String listString = "";
        for (int s : listCode) {
            listString += s;
        }
        return listString;
    }

    /**
     * Mencari bit asli dari Hammingcode.
     * @param code hamming code
     * @return string bit alsi
     */
    public static String decode(String code) {
        //Merubah string menjadi arrayList
        ArrayList<Integer> listCode = new ArrayList<Integer>();
        listCode = stringToList(code);

        //Find amount of redundan from hammingcode
        int redundan = 0;
        while (Math.pow(2, redundan) < code.length() + 1) {
            redundan++;
        }

        int resultParity;
        int index;
        int start;
        int falseParity = 0;

        //Check Parity
        //Looping sebanyak redundannya
        for (int r = 0, jarak = 1; r < redundan; r++, jarak *= 2) {
            //Restart result
            resultParity = 0;

            //inisiasi index
            start = (int) Math.pow(2, r);
            index = start - 1;

            //Menghitung Parity
            while (listCode.size() > index) {
                //Menjumlah parity kedalam result sejumlah jaraknya
                for (int i = 0; i < jarak; i++) {
                    if (listCode.size() > index) {
                        resultParity += listCode.get(index);
                        index++;
                    }
                }
                //Skip bit sejumlah jaraknya
                for (int i = 0; i < jarak; i++) {
                    index += 1;
                }
            }
            if (resultParity % 2 != 0) {
                falseParity += start;
            }
        }

        //Memperbaiki bit yang salah
        if (falseParity != 0) {
            if (listCode.get(falseParity - 1) == 1) {
                listCode.set(falseParity - 1, 0);
            } else {
                listCode.set(falseParity - 1, 1);
            }
        }

        //remove parity
        for (int i = redundan - 1; i >= 0; i--) {
            double p = Math.pow(2, i);
            listCode.remove((int)p - 1);
        }

        //Merubah ArrayList to String
        String listString = "";
        for (int s : listCode) {
            listString += s;
        }

        return listString;
    }


    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
