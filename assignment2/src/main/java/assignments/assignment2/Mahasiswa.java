package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    /**
     * Membuat constructor untuk Mahasiswa.
     * Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
     * @param npm NPM mahasiswa.
     * @param nama nama mahasiswa.
     * @param komponenPenilaian list komponen penilaian.
     */
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    /**
     * Mengembalikan KomponenPenilaian yang bernama namaKomponen.
     * Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
     * @param namaKomponen nama komponen penilaian.
     * @return nama nama dari komponen penilaian.
     */
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (KomponenPenilaian nama : komponenPenilaian) {
            if (nama != null) {
                if (nama.getNama().equals(namaKomponen)) {
                    return nama;
                }
            }
        }
        return null;
    }

    /**
     * Mengembalikan NPM mahasiswa.
     * @return npm NPM mahasiswa.
     */
    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilai nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    /**
     * Menghitung nilai akhir dari seorang mahasiswa.
     * @return nilaiAkhir
     */
    private double nilaiAkhir() {
        double nilaiAkhir = 0;
        for (KomponenPenilaian komponen : komponenPenilaian) {
            if (komponen != null) {
                nilaiAkhir += komponen.getNilai();
            }
        }
        return nilaiAkhir;
    }

    /**
     * Mengembalikan rekapan sesuai dengan permintaan soal.
     * @return result dari rekap.
     */
    public String rekap() {
        String result = "";
        String nilaiAkhir = String.format("%.2f", nilaiAkhir());

        for (KomponenPenilaian komponen : komponenPenilaian) {
            if (komponen != null) {
                result += komponen + "\n";
            }
        }

        result += ("Nilai Akhir: " + nilaiAkhir + "\n");
        result += ("Huruf: " + getHuruf(nilaiAkhir()) + "\n");
        result += getKelulusan(nilaiAkhir());
        return result;
    }

    /**
     * Mengembalikan representasi String dari Mahasiswa sesuai permintaan soal.
     * @return string
     */
    public String toString() {
        return String.format("%s - %s", npm, nama);
    }

    /**
     * Megembalikan detail dari Mahasiswa sesuai permintaan soal.
     * @return result.
     */
    public String getDetail() {
        String result = "";
        String nilaiAkhir = String.format("%.2f", nilaiAkhir());

        for (KomponenPenilaian komponen : komponenPenilaian) {
            if (komponen != null) {
                result += komponen.getDetail() + "\n\n";
            }
        }

        result += "Nilai akhir: " + nilaiAkhir + "\n";
        result += "Huruf: " + getHuruf(nilaiAkhir()) + "\n";
        result += getKelulusan(nilaiAkhir());

        return result;
    }

    /**
     * Mendefinisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
     * Hint: bandingkan NPM-nya, String juga punya method compareTo.
     */
    @Override
    public int compareTo(Mahasiswa other) {
        return npm.compareTo(other.getNpm());
    }
}
