package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    /**
     * Membuat constructor untuk AsistenDosen.
     * @param kode kode dari asdos.
     * @param nama nama dari asdos.
     */
    public AsistenDosen(String kode, String nama) {
        this.nama = nama;
        this.kode = kode;
    }

    /**
     * Mengembalikan kode AsistenDosen.
     * @return kode kode dari asisten dosen.
     */
    public String getKode() {
        return kode;
    }

    /**
     * Menambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
     * Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
     * Note: manfaatkan method compareTo pada Mahasiswa.
     * @param mahasiswa mahasiswa yang ingin ditambah.
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    /**
     * kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
     * @param npm NPM mahasiswa.
     * @return mahasiswa yang sesuai dengan NPM yang dicari.
     */
    public Mahasiswa getMahasiswa(String npm) {
        for (Mahasiswa murid : mahasiswa) {
            if (murid.getNpm().equals(npm)) {
                return murid;
            }
        }
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return null;
    }

    /**
     * Method untuk merekap mahasiswa yang ada pada asdos.
     * @return result sesuai dengan soal.
     */
    public String rekap() {
        String result = "";
        int count = 0;
        for (Mahasiswa murid : mahasiswa) {
            if (murid != null) {
                result += murid + "\n";
                result += murid.rekap() + "\n\n";
                count++;
            }
        }
        if (count != 0) {
            result = result.substring(0, result.length() - 2);
        }
        return result;
    }

    /**
     * Method untuk menghapus mahasiswa tertentu.
     * @param npm NPM dari mahasiswa.
     */
    public void removeMahasiswa(String npm) {
        Mahasiswa mahasiswa = getMahasiswa(npm);
        this.mahasiswa.remove(mahasiswa);
    }

    /**
     * Mengembalikan kode dan nama asdos sesuai soal.
     * @return result sesuai dengan soal.
     */
    public String toString() {
        String result = String.format("%s - %s", kode, nama);
        return result;
    }
}
