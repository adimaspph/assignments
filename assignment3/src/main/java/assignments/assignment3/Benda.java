package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    /**
     * Contructor, memanggil superclass
     * @param name nama
     * @param tipe job
     */
    public Benda(String name, String tipe){
        // TODO: Buat constructor untuk Benda.
        // Hint: Akses constructor superclass-nya
        super(name, tipe);
    }

    public abstract void tambahPersentase();

    /**
     * getter persentase menular
     * @return
     */
    public int getPersentaseMenular(){
        // TODO : Kembalikan nilai dari atribut persentaseMenular
        return persentaseMenular;
    }

    /**
     * setter dari persentasi penularan
     * @param persentase integer
     */
    public void setPersentaseMenular(int persentase) {
        // TODO : Gunakan sebagai setter untuk atribut persentase menular
        if (persentase >= 100) {
            persentaseMenular = 100;
        } else if (persentase <= 0) {
            persentaseMenular = 0;
        } else {
            persentaseMenular = persentase;
        }

        if (persentaseMenular >= 100) {
            this.ubahStatus("Positif");
        } else {
            this.ubahStatus("Negatif");
        }
    }

    /**
     * Mengembalikan string jika carrier di panggil sesuai dengan soal
     * @return string
     */
    @Override
    public String toString() {
        return String.format("%s %s", getTipe(), getNama());
    }
}