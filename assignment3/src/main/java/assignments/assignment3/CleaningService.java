package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    /**
     * Constructor, memanggil super class
     * @param nama nama
     */
    public CleaningService(String nama){
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama, "CLEANING SERVICE");
    }

    /**
     * Mengubah benda menjadi negatif dan mereset persentase dari benda, serta mengurangi aktif kasus dari penularnya
     * @param benda benda
     */
    public void bersihkan(Benda benda){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        for (Carrier carrier : benda.getRantaiPenular()) {
            if (carrier != benda)
                carrier.kurangiKasus();
        }
        benda.setPersentaseMenular(0);
        benda.ubahStatus("Negatif");
        jumlahDibersihkan++;
    }

    /**
     * getter jumlah yang telah dibersihkan cleaning service
     * @return
     */
    public int getJumlahDibersihkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

}