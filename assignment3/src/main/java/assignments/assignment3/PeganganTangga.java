package assignments.assignment3;

public class PeganganTangga extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda

    /**
     * Constructor, memanggil super class
     * @param name nama
     */
    public PeganganTangga(String name){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name, "PEGANGAN TANGGA");
    }

    /**
     * Menambah persentase dari benda jika terpegang oleh manusia positif
     */
    @Override
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
    }
    
}