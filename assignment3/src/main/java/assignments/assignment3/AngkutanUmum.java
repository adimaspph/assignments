package assignments.assignment3;

public class AngkutanUmum extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda

    /**
     * Constructor, memanggil super class
     * @param name nama
     */
    public AngkutanUmum(String name){
        // TODO: Buat constructor untuk AngkutanUmum.
        // Hint: Akses constructor superclass-nya
        super(name, "ANGKUTAN UMUM");
    }

    /**
     * Menambah persentase dari benda jika terpegang oleh manusia positif
     */
    @Override
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 35);
    }




}