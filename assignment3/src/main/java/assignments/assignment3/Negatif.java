package assignments.assignment3;

public class Negatif implements Status{

    /**
     * getter status covid
     * @return negatif
     */
    public String getStatus(){
        return "Negatif";
    }

    /**
     * Mengubah status tertular menjadi positif dan memasukan rantai penular ke tertular
     * @param penular carrier
     * @param tertular carrier
     */
    public void tularkan(Carrier penular, Carrier tertular){
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
        tertular.ubahStatus("Positif");

        for (Carrier carrier : penular.getRantaiPenular()) {
            carrier.tambahKasus();
            tertular.getRantaiPenular().add(carrier);
        }
        tertular.getRantaiPenular().add(tertular);
    }
}