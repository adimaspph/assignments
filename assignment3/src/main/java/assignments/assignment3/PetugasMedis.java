package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    /**
     * Constructor, memanggil super class
     * @param nama nama
     */
    public PetugasMedis(String nama){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama, "PETUGAS MEDIS");
    }

    /**
     * Mengubah carrier yang positif menjadi negatif, serta mengurangi aktif kasus dari penularnya
     * @param manusia
     */
    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        if (manusia.getStatusCovid().equals("Positif")) {
            for (Carrier carrier : manusia.getRantaiPenular()) {
                if (!(carrier.getNama().equals(manusia.getNama()))) {
                    carrier.kurangiKasus();
                }
            }
            manusia.ubahStatus("Negatif");
            manusia.tambahSembuh();
        }
        jumlahDisembuhkan++;
    }

    /**
     * getter dari banyaknya manusia yang telah di sembuhkan
     * @return jumlahDisembuhkan
     */
    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }
}