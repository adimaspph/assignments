package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    /**
     * Conctructor, mengassign nama, tipe, status, dan membuat list rantai penularan.
     * @param nama nama
     * @param tipe tipe job
     */
    public Carrier(String nama,String tipe){
        // TODO: Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        rantaiPenular = new ArrayList<>();
    }

    /**
     * Getter nama
     * @return nama
     */
    public String getNama(){
        // TODO : Kembalikan nilai dari atribut nama
        return nama;
    }

    /**
     * Getter tipe
     * @return tipe
     */
    public String getTipe(){
        // TODO : Kembalikan nilai dari atribut tipe
        return tipe;
    }

    /**
     * getter status
     * @return (positif/negatif)
     */
    public String getStatusCovid(){
        // TODO : Kembalikan nilai dari atribut statusCovid
        return statusCovid.getStatus();
    }

    /**
     * getter Aktif kasus
     * @return banyaknya orang yang masih positif karena carrier tersebut.
     */
    public int getAktifKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return aktifKasusDisebabkan;
    }

    /**
     * getter total kasus
     * @return banyaknya orang yang terinfkesi karena carrier tersebut.
     */
    public int getTotalKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return totalKasusDisebabkan;
    }

    /**
     * getter rantai
     * @return rantai penularan dari carrier.
     */
    public List<Carrier> getRantaiPenular(){
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    /**
     * Menambah total dan aktif kasus
     */
    public void tambahKasus() {
        totalKasusDisebabkan++;
        aktifKasusDisebabkan++;
    }

    /**
     * Mengurangi aktif kasus
     */
    public void kurangiKasus() {
        this.aktifKasusDisebabkan--;
    }

    /**
     * Mengubah status carrier sesuai param
     * @param status positif/negatif
     */
    public void ubahStatus(String status){
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (statusCovid.getStatus().equals("Positif")  && status.equals("Negatif")) {
            statusCovid = new Negatif();
            rantaiPenular.clear();

        } else if (statusCovid.getStatus().equals("Negatif") && status.equals("Positif")) {
            statusCovid = new Positif();
        }
    }

    /**
     * Mengecek interaksi antara Manusia atau benda.
     * Mengecek apakah berinteraksi dengan positif atau tidak.
     * @param lain carrier
     */
    public void interaksi(Carrier lain) {
        // TODO : Objek ini berinteraksi dengan objek lain

        if (this instanceof Manusia && lain instanceof Manusia) {
            if (this.getStatusCovid().equals("Negatif") && lain.getStatusCovid().equals("Positif")) {
                lain.statusCovid.tularkan(lain,this);

            } else if (this.getStatusCovid().equals("Positif") && lain.getStatusCovid().equals("Negatif")) {
                this.statusCovid.tularkan(this, lain);
            }

        } else if (this instanceof Manusia && lain instanceof Benda) {
            if (this.getStatusCovid().equals("Positif")) {
                ((Benda) lain).tambahPersentase();
                if (lain.getStatusCovid().equals("Positif")) {
                    this.statusCovid.tularkan(this, lain);
                }

            } else if (lain.getStatusCovid().equals("Positif")) {
                lain.statusCovid.tularkan(lain,this);
            }

        } else if (this instanceof Benda && lain instanceof Manusia) {
            if (this.getStatusCovid().equals("Positif")) {
                this.statusCovid.tularkan(this, lain);

            } else if (lain.getStatusCovid().equals("Positif")) {
                ((Benda) this).tambahPersentase();
                if (this.getStatusCovid().equals("Positif")) {
                    this.statusCovid.tularkan(lain, this);
                }
            }
        }
    }


    public abstract String toString();

}
