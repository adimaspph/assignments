package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh;

    /**
     * memanggil super class
     * @param nama nama
     * @param tipe job
     */
    public Manusia(String nama, String tipe){
        // TODO: Buat constructor untuk Manusia.
        // Hint: Akses constructor superclass-nya
        super(nama, tipe);
    }

    /**
     * menambah attribut jumlah sembuh
     */
    public void tambahSembuh(){
        // TODO: Fungsi untuk menambahkan nilai pada atribut jumlahSembuh.
        // Hint: Perhatikan bahwa access modifiernya bertipe public
        jumlahSembuh += 1;
    }

    /**
     * Getter jumlah sembuh
     * @return jumlah sembuh
     */
    public static int getJumlahSembuh() {
        // TODO: Kembalikan nilai untuk atribut jumlahSembuh.
        return jumlahSembuh;
    }

    /**
     * Mengubah status menjadi positif jika carrier terinfeksi tanpa sebab
     */
    public void terinfeksi() {
        this.ubahStatus("Positif");
        getRantaiPenular().add(this);
    }

    /**
     * Mengembalikan string jika carrier di panggil sesuai dengan soal
     * @return string
     */
    @Override
    public String toString() {
        return String.format("%s %s", getTipe(), getNama());
    }
}