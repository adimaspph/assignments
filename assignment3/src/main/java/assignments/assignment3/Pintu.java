package assignments.assignment3;

public class Pintu extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda  

    /**
     * Constructor, memanggil super class
     * @param name nama
     */
    public Pintu(String name){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        super(name, "PINTU");
    }

    /**
     * Menambah persentase dari benda jika terpegang oleh manusia positif
     */
    @Override
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 30);
    }
}