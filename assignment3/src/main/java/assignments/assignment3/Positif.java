package assignments.assignment3;


import java.util.ArrayList;

public class Positif implements Status{

    /**
     * getter status covid
     * @return positif
     */
    public String getStatus(){
        return "Positif";
    }

    /**
     * Mengubah status tertular menjadi positif dan memasukan rantai penular ke tertular
     * @param penular carrier
     * @param tertular carrier
     */
    public void tularkan(Carrier penular, Carrier tertular){
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
        ArrayList<Carrier> temp = new ArrayList<>();

        tertular.ubahStatus("Positif");

        for (Carrier carrier : penular.getRantaiPenular()) {
            if (!temp.contains(carrier)){
                carrier.tambahKasus();
            }
            temp.add(carrier);
            tertular.getRantaiPenular().add(carrier);
        }
        tertular.getRantaiPenular().add(tertular);
    }
}