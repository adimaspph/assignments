package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    /**
     * Constructor, Membuat list untuk list carrier
     */
    public World(){
        // TODO: Buat constructor untuk class World
        this.listCarrier = new ArrayList<>();
    }

    /**
     * Membuat carrier sesuai tipe yang diinginkan
     * @param tipe job carrier
     * @param nama nama carrier
     * @return
     */
    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        Carrier result = null;
        String type = tipe.toLowerCase();

        if (type.equals("angkutan_umum")) {
            result = new AngkutanUmum(nama);
        } else if (type.equals("cleaning_service")) {
            result = new CleaningService(nama);
        } else if (type.equals("jurnalis")) {
            result = new Jurnalis(nama);
        } else if (type.equals("ojol")) {
            result = new Ojol(nama);
        } else if (type.equals("pegangan_tangga")) {
            result = new PeganganTangga(nama);
        } else if (type.equals("pekerja_jasa")) {
            result = new PekerjaJasa(nama);
        } else if (type.equals("petugas_medis")) {
            result = new PetugasMedis(nama);
        } else if (type.equals("pintu")) {
            result = new Pintu(nama);
        } else if (type.equals("tombol_lift")) {
            result = new TombolLift(nama);
        }
        listCarrier.add(result);

        return result;
    }

    /**
     * Mengembalikan objek carrier sesuai dengan nama
     * @param nama nama carrier
     * @return carrier atau null
     */
    public Carrier getCarrier(String nama){
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier carrier : listCarrier) {
            if (carrier.getNama().equals(nama)) {
                return carrier;
            }
        }
        return null;
    }
}
