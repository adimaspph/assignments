package assignments.assignment3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

public class SimulatorGUI extends Application {

    private Stage window;
    private Scene home, setFile, querry, result, terminal;
    private InputOutput io;
    private String inputType;
    private String outputType;
    private String inputPath = null;
    private String outPath = null;


    /**
     * Menampilkan scene awal, yoitu memilih tipe input dan output
     * @param primaryStage Stage
     * @throws Exception Error
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;


        Label greeting = new Label("Selamat Datang di Corona Virus Simulator!!!!");
        greeting.setStyle("-fx-font-weight: bold ;" +
                "-fx-font-size: 20px");

        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label l = new Label("By Adimas P. Pratama, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");

        VBox parent = new VBox(greeting);
        parent.getChildren().add(l);
        parent.setAlignment(Pos.CENTER);
        parent.setSpacing(20);


        HBox input = new HBox(new Label("Input :  "));
        input.setAlignment(Pos.CENTER);
        ChoiceBox<String> CInput = new ChoiceBox<>();
        CInput.getItems().add("JavaFx GUI");
        CInput.getItems().add("File");
//        CInput.getItems().add("Terminal");
//        CInput.setValue("JavaFx GUI");
        input.getChildren().add(CInput);
        parent.getChildren().add(input);

        HBox output = new HBox(new Label("Output :  "));
        output.setAlignment(Pos.CENTER);
        ChoiceBox<String> COutput = new ChoiceBox<>();

        Button btn = new Button("Next");

        CInput.setOnAction(e -> {
            if (CInput.getValue().equalsIgnoreCase("JavaFx GUI")){
                COutput.getItems().clear();
                COutput.getItems().add("JavaFx GUI");
                COutput.setValue("JavaFx GUI");
            } else {
                COutput.getItems().clear();
                COutput.getItems().add("File");
                COutput.getItems().add("Terminal");
                COutput.setValue("Terminal");
            }
            try {
                output.getChildren().add(COutput);
                parent.getChildren().add(output);
                parent.getChildren().add(btn);
            } catch (Exception x){

            }
        });

        btn.setOnAction(e -> getInputOutput(CInput, COutput));

        home = new Scene(parent, 700, 550);

        window.setTitle("TP3 Covid Simulator GUI - Adimas");
        window.setScene(home);
        window.show();
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Method yang mensortir tipe yang dipilih user
     * @param input choiceBox
     * @param output choicebox
     */
    private void getInputOutput(ChoiceBox<String> input,ChoiceBox<String> output){
        if (input.getValue().equalsIgnoreCase("file")){
            inputType = "file";
        } else if (input.getValue().equalsIgnoreCase("JavaFx GUI")){
            inputType = "JavaFx GUI";
        } else {
            inputType = "terminal";
        }

        if (output.getValue().equalsIgnoreCase("file")){
            outputType = "file";
        } else if (output.getValue().equalsIgnoreCase("JavaFx GUI")){
            outputType = "JavaFx GUI";
        } else {
            outputType = "terminal";
        }

        if (inputType.equalsIgnoreCase("file") || outputType.equalsIgnoreCase("file")) {
            fileScene();
        } else if (inputType.equalsIgnoreCase("JavaFx GUI")){
            System.out.println("Pindah ke querry");
            io = new InputOutput(inputType, "NOTHING", outputType, outPath);
            querry();
        } else {
            io = new InputOutput(inputType, "NOTHING", outputType, outPath);
            setTerminal();
            try {
                io.run();
            } catch (Exception v){
                v.printStackTrace();
            }
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Scene dimana akan mengarahkan user untuk memilih file yang akan menjadi input ataupun output
     */
    private void fileScene(){
        VBox parent = new VBox(new Label("Select File"));
        parent.setAlignment(Pos.CENTER);
        parent.setSpacing(20);

        Button btnFile = new Button("Select Input File");
        if (inputType.equalsIgnoreCase("file"))
            parent.getChildren().add(btnFile);
        btnFile.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                FileChooser fc = new FileChooser();
                fc.setTitle("Selecet the input file");
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Text Files", "*txt"));
                File selectedFiles = fc.showOpenDialog(null);
                if (selectedFiles != null) {
                    inputPath = selectedFiles.getAbsolutePath();
                    System.out.println(inputPath);
                }
            }
        });

        Button btnOut = new Button("Select Output File");
        Label bikinFileText = new Label("Silahkan buat file .txt dahulu jika anda belum mempunyai File Output");
        if (outputType.equalsIgnoreCase("file")){
            parent.getChildren().add(btnOut);
            parent.getChildren().add(bikinFileText);
        }
        btnOut.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                FileChooser fc = new FileChooser();
                fc.setTitle("Selecet the Output file");
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Text Files", "*txt"));
                File selectedFiles = fc.showOpenDialog(null);
                if (selectedFiles != null) {
                    outPath = selectedFiles.getAbsolutePath();
                    System.out.println(outPath);
                }
            }
        });

        Button next = new Button("Next");
        parent.getChildren().add(next);
        Label peringatan = new Label("Masukkan File!!!");
        peringatan.setStyle("-fx-text-fill: red;");
        next.setOnAction(e -> {
            if (inputType.equals("file") && outputType.equals("file")){
                if (inputPath != null && outPath != null) {
                    io = new InputOutput("text" , inputPath, "text", outPath);
                    try {
                        io.run();
                    } catch (Exception v){
                        v.printStackTrace();
                    }
                    window.close();
                }
            } else if (inputType.equals("file")){
                if ((inputPath != null)) {
                    if (outputType.equalsIgnoreCase("terminal")) {
                        //System.out.println("File - Terminal");
                        io = new InputOutput("text", inputPath, outputType, "NOTHING");
                        try {
                            io.run();
                        } catch (Exception v){
                            v.printStackTrace();
                        }
                        window.close();
                    }
                }
            } else if (outputType.equals("file")){
                if (outPath != null) {
                    if (inputType.equalsIgnoreCase("terminal")) {
                        //System.out.println("terminal - file");
                        io = new InputOutput(inputType, "NOTHING", "text", outPath);
                        try {
                            io.run();
                        } catch (Exception v){
                            v.printStackTrace();
                        }
                        setTerminal();
                    }
                }
            }
            try {
                parent.getChildren().add(peringatan);
            } catch (Exception ignored) { }
        });

        setFile = new Scene(parent, 700, 550);
        window.setScene(setFile);
    }


//----------------------------------------------------------------------------------------------------------------------------------------------

    private String[] toRole = {"ANGKUTAN_UMUM", "cleaning_service", "jurnalis", "ojol", "pegangan_tangga", "pekerja_jasa", "petugas_medis", "pintu", "tombol_lift"};

    /**
     * Menampilkan Querry apa saja yang bisa dilakukan oleh user. dan menampilkan hasil dari Querry yang ingin di simulasikan oleh user
     */
    private void querry(){
        VBox parent = new VBox();
        Label title = new Label("Pilih Perintah yang Akan Disimulasikan");
        title.setStyle("-fx-font-weight: bold ;" +
                "-fx-font-size: 20px");
        parent.getChildren().add(title);
        parent.setSpacing(20);
        parent.setAlignment(Pos.CENTER);

        ArrayList<String> listPerintah = new ArrayList<>();

        Boolean medis = false;
        Boolean cleaning = false;

        for (Carrier carrier : io.getWorld().listCarrier) {
            if (carrier instanceof PetugasMedis){
                medis = true;
            } else if ( carrier instanceof CleaningService){
                cleaning = true;
            }
        }

        if (medis) {
            listPerintah.add("SEMBUHKAN");
            listPerintah.add("TOTAL_SEMBUH_PETUGAS_MEDIS");
        } else if (cleaning){
            listPerintah.add("BERSIHKAN");
            listPerintah.add("TOTAL_BERSIH_CLEANING_SERVCE");
        }

        if (io.getWorld().listCarrier.size() >= 2)
            listPerintah.add("INTERAKSI");
        if (io.getWorld().listCarrier.size() >= 1) {
            listPerintah.add("POSITIFKAN");
            listPerintah.add("RANTAI");
            listPerintah.add("TOTAL_KASUS_DARI_OBJEK");
            listPerintah.add("AKTIF_KASUS_DARI_OBJEK");
            listPerintah.add("TOTAL_SEMBUH_MANUSIA");
        }
        listPerintah.add("ADD");
        listPerintah.add("EXIT");


        HBox kata1 = new HBox(new Label("Pilih Perintah: "));
        kata1.setSpacing(10);
        kata1.setAlignment(Pos.CENTER);
        ChoiceBox<String> perintah = new ChoiceBox(FXCollections.observableArrayList(listPerintah));
        kata1.getChildren().add(perintah);
        parent.getChildren().add(kata1);

        HBox kata2 = new HBox();
        kata2.setSpacing(10);
        kata2.setAlignment(Pos.CENTER);
        kata1.getChildren().add(kata2);

        //Textfield nama objek
        TextField nama = new TextField();
        nama.setPromptText("Nama");

        //List Role
        for (int i = 0; i < toRole.length; i++) {
            toRole[i] = toRole[i].toUpperCase();
        }
        ChoiceBox<String> role = new ChoiceBox(FXCollections.observableArrayList(toRole));

        //List Carrier
        ArrayList<String> carriers = new ArrayList<>();
        for (Carrier carrier : io.getWorld().listCarrier){
            carriers.add(carrier.getNama());
        }
        ChoiceBox objek1 = new ChoiceBox(FXCollections.observableArrayList(carriers));
        ChoiceBox<String> objek2 = new ChoiceBox(FXCollections.observableArrayList(carriers));

        //List Medis
        ArrayList<String> listMedis = new ArrayList<>();
        for (Carrier carrier : io.getWorld().listCarrier){
            if (carrier instanceof PetugasMedis) {
                listMedis.add(carrier.getNama());
            }
        }
        ChoiceBox CBMedis = new ChoiceBox(FXCollections.observableArrayList(listMedis));

        //List Cleaning Service
        ArrayList<String> listCleaningService = new ArrayList<>();
        for (Carrier carrier : io.getWorld().listCarrier){
            if (carrier instanceof CleaningService) {
                listCleaningService.add(carrier.getNama());
            }
        }
        ChoiceBox CBCleaningService = new ChoiceBox(FXCollections.observableArrayList(listCleaningService));



        perintah.setOnAction(e -> {
            if (perintah.getValue().equals("ADD")) {
                kata2.getChildren().clear();
                kata2.getChildren().add(role);
                kata2.getChildren().add(nama);
            } else if (perintah.getValue().equals("EXIT")){
                kata2.getChildren().clear();
            } else if (perintah.getValue().equals("INTERAKSI")){
                kata2.getChildren().clear();
                kata2.getChildren().add(objek1);
                kata2.getChildren().add(objek2);
            } else if (perintah.getValue().equals("POSITIFKAN")){
                kata2.getChildren().clear();
                kata2.getChildren().add(objek1);
            } else if (perintah.getValue().equals("SEMBUHKAN")){
                kata2.getChildren().clear();
                kata2.getChildren().add(CBMedis);
                kata2.getChildren().add(objek2);
            } else if (perintah.getValue().equals("BERSIHKAN")){
                kata2.getChildren().clear();
                kata2.getChildren().add(CBCleaningService);
                kata2.getChildren().add(objek2);
            } else if (perintah.getValue().equals("RANTAI")){
                kata2.getChildren().clear();
                kata2.getChildren().add(objek1);
            } else if (perintah.getValue().equals("TOTAL_KASUS_DARI_OBJEK")){
                kata2.getChildren().clear();
                kata2.getChildren().add(objek1);
            } else if (perintah.getValue().equals("AKTIF_KASUS_DARI_OBJEK")){
                kata2.getChildren().clear();
                kata2.getChildren().add(objek1);
            } else if (perintah.getValue().equals("TOTAL_SEMBUH_MANUSIA")){
                kata2.getChildren().clear();
            } else if (perintah.getValue().equals("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                kata2.getChildren().clear();
                kata2.getChildren().add(CBMedis);
            }
        });


        //Submit Button
        Button submit = new Button("Simulasikan");
        parent.getChildren().add(submit);

        //label Result
        Label lResult = new Label("HISTORI");
        lResult.setPrefWidth(540);
//        parent.getChildren().add(lResult);

        //Text Result
        ScrollPane bawah = new ScrollPane();
        Label result = new Label(io.toGUI);

        bawah.setMaxSize(600, 350);
        bawah.setPrefViewportHeight(300);
        bawah.setPadding(new Insets(15));
        bawah.setContent(result);
        parent.getChildren().add(bawah);


        submit.setOnAction(e -> {
            try {
                String command = "";
                switch (perintah.getValue()) {
                    case "ADD":
                        command = String.format("%s %s %s", perintah.getValue(), role.getValue(), nama.getText());
                        break;
                    case "EXIT":
                        window.close();
                        break;
                    case "INTERAKSI":
                        command = String.format("%s %s %s", perintah.getValue(), objek1.getValue(), objek2.getValue());
                        break;
                    case "POSITIFKAN":
                    case "RANTAI":
                    case "TOTAL_KASUS_DARI_OBJEK":
                    case "AKTIF_KASUS_DARI_OBJEK":
                        command = String.format("%s %s", perintah.getValue(), objek1.getValue());
                        break;
                    case "SEMBUHKAN":
                        command = String.format("%s %s %s", perintah.getValue(), CBMedis.getValue(), objek2.getValue());
                        break;
                    case "BERSIHKAN":
                        command = String.format("%s %s %s", perintah.getValue(), CBCleaningService.getValue(), objek2.getValue());
                        break;
                    case "TOTAL_SEMBUH_MANUSIA":
                        command = String.format("%s", perintah.getValue());
                        break;
                    case "TOTAL_SEMBUH_PETUGAS_MEDIS":
                        command = String.format("%s %s", perintah.getValue(), CBMedis.getValue());
                        break;
                }

                System.out.println(command);
                io.GUI(command);
                querry();
            } catch (Exception ignored){ }
        });

        querry = new Scene(parent, 700, 550);
        window.setScene(querry);
    }



//----------------------------------------------------------------------------------------------------------------------------------------------


    /**
     * Menunjukan user untuk membuka terminal, karena GUI sudah tidak dipakai lagi
     */
    private void setTerminal(){
        VBox parent = new VBox();
        Label pindah = new Label("Silahkan Pindah ke Terminal");
        pindah.setStyle("-fx-font-weight: bold;" +
                "-fx-font-size: 40px");
        parent.setAlignment(Pos.CENTER);

        Button ok = new Button("Mengerti!!!");
        ok.setOnAction(e -> {
            ((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);
        });

        parent.getChildren().add(pindah);
        parent.getChildren().add(ok);

        terminal = new Scene(parent, 700, 550);
        window.setScene(terminal);
    }

    /**
     * Main Method
     * @param args Args
     */
    public static void main(String[] args) {
        launch(args);
    }

}
